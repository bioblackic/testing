package info.androidhive.slidingmenu;

import java.io.InputStream;




import java.util.ArrayList;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;

import com.bravolol.languagesources.*;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> word;
    private ArrayList<String> translate;
    
    private RelativeLayout row;

    	

    public MyAdapter(Context context) {
        this.context = context;
        
    }
    
    public MyAdapter(Context context,ArrayList<String> word,ArrayList<String> trans)
    {
    	translate = trans;
    	this.context = context;
    	this.word = word;
    }

    public int getCount() {
        return word.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv,tv2;
      //  if (convertView == null) {
        	
        	row = (RelativeLayout) (convertView == null
                    ? LayoutInflater.from(context).inflate(R.layout.grid_content, parent, false)
                    : convertView);
        	row.setBackgroundResource(R.drawable.back);
        	row.setLayoutParams(new GridView.LayoutParams(500, 350));
         ((TextView)row.findViewById(R.id.TxtName)).setText(word.get(position));
         ((TextView)row.findViewById(R.id.TxtName)).setTextColor(Color.parseColor("#60C3AC"));
         ((TextView)row.findViewById(R.id.TxtName)).setTextSize(20);
         ((TextView)row.findViewById(R.id.TxtName)).setGravity(Gravity.CENTER);
         ((TextView)row.findViewById(R.id.TxtPackage)).setText(translate.get(position));
         ((TextView)row.findViewById(R.id.TxtPackage)).setTextColor(Color.parseColor("#60C3AC"));
         ((TextView)row.findViewById(R.id.TxtPackage)).setTextSize(15);
         ((TextView)row.findViewById(R.id.TxtPackage)).setGravity(Gravity.CENTER);
        /*	
            tv = new TextView(context);
            //tv.setLayoutParams(new GridView.LayoutParams(500, 500));
            tv.setPadding(100, 10, 10, 10);
            tv.setTextColor(Color.parseColor("#60C3AC"));
            tv.setBackgroundResource(R.drawable.back);
            tv.setTextSize(5);
            tv.setGravity(Gravity.CENTER);
            
            tv2 = new TextView(context);
            //tv2.setLayoutParams(new GridView.LayoutParams(500, 500));
            tv2.setPadding(100, 10, 10, 10);
            tv2.setTextColor(Color.parseColor("#60C3AC"));
            tv2.setBackgroundResource(R.drawable.back);
            tv2.setTextSize(15);
            tv2.setGravity(Gravity.CENTER);
            
            */
        //}
       /* else {
            tv = (TextView) convertView;
            tv2 = (TextView) convertView;

        }*/

       //     tv.setText(word.get(position));
        //    tv2.setText(word.get(position));

            //texts.setLayoutParams(new GridView.LayoutParams(500, 500));
        return row;
    }
}