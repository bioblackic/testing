package info.androidhive.slidingmenu;

import java.io.InputStream;
import java.util.ArrayList;

import com.bravolol.languagesources.Decoder;
import com.bravolol.languagesources.PhraseBean;
import com.bravolol.languagesources.ScenarioBean;
import com.bravolol.languagesources.TermBean;

import android.app.Fragment;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

public class CommunityFragment extends Fragment {
	
	public CommunityFragment(){}
	
	//String[] wordList;
	ArrayList<String> words;
	ArrayList<String> wordList = new ArrayList<String>();
	ArrayList<String> translate = new ArrayList<String>();

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_community, container, false);
         
 
        
        return rootView;
    }
	
	public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        wordLoad();
        GridView gridview = (GridView) getActivity().findViewById(R.id.gridview);
        gridview.setAdapter(new MyAdapter(getActivity(),wordList,translate));

        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(getActivity(), "" + position, Toast.LENGTH_SHORT).show();
            }
        });
       // ListView listView = (ListView)getActivity().findViewById(R.id.lvExp);

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),R.layout.single_list_item_view, R.id.product_label, values);

        //listView.setAdapter(adapter);

    }

/*Peter's remind:
 * This is the testing of word loader from the words library. 
 * It should be loaded immediately when the activity was created.
 * Please move it to the main activity and create several dynamic arrays to store different types of data.
 */
	private void wordLoad() {
		// TODO Auto-generated method stub
			AssetManager assetManager = getActivity().getAssets();
			InputStream inputAsset;

			try {
				
				inputAsset = assetManager.open("phrasebookDemo.bin");

				// When the constructor loaded, all data will be decoded
				// from the data file and stored into PhraseBean object
				Decoder decoder = new Decoder(inputAsset);

				// Get the decoded data sources
				PhraseBean phraseBean = decoder.getPhraseBean();

				//////////////////////////////////Example to print out the data //////////////////////////////////
				Log.d("MainActivity", "Scenario count:***" + phraseBean.getScenarioBeanArray().length + "***");

				for (int i = 0; i < phraseBean.getScenarioBeanArray().length; i++) {
					ScenarioBean scenarioBean = phraseBean.getScenarioBeanArray()[i];

					Log.d("MainActivity", "Scenario:[" + i + "]" + scenarioBean.getTitle() + "|");

					Log.d("MainActivity", "     Terms count:***" + scenarioBean.getTermBeanArray().length + "***");

					for (int k = 0; k < scenarioBean.getTermBeanArray().length; k++) {
						TermBean termBean = scenarioBean.getTermBeanArray()[k];
						Log.d("MainActivity", "     Terms:[" + termBean.getTid() + "]" + 
								termBean.getSentence() + "|" + termBean.getTranslate() + "|");
						wordList.add(termBean.getSentence());
						translate.add(termBean.getTranslate());
					}
				}
				//////////////////////////////////Example to print the data //////////////////////////////////
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		
	}  
}
