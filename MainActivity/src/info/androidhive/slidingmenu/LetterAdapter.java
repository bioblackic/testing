package info.androidhive.slidingmenu;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class LetterAdapter extends BaseAdapter {

	private boolean positionKey;
	private RelativeLayout gridLetter;
	//store letters
	private String[] letters = {"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z"};
	private String[] lettersBottom = {"X","C","V","B","N","M"};
	//inflater for button layout
	private LayoutInflater letterInf;

	public LetterAdapter(Context c,boolean a) {

		//specify layout to inflate
		letterInf = LayoutInflater.from(c);
		positionKey = a;
		Log.d("Letter","constructor");
	}

	@Override
	public int getCount() {
		if(positionKey){
		return letters.length;}
		else{
			return lettersBottom.length;
		}
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//create a button for the letter at this position in the alphabet
		Button letterBtn;
		/*
		if (convertView == null) {  
			//inflate the button layout
			letterBtn = (Button)letterInf.inflate(R.layout.letter, parent, false);
		} else {
			letterBtn = (Button) convertView;
		}
		//set the text to this letter
		letterBtn.setText(letters[position]);
		return letterBtn;*/
		if(positionKey){
		gridLetter = (RelativeLayout) (convertView == null
                ? letterInf.inflate(R.layout.letter_adapter, parent, false)
                : convertView);
			((Button)gridLetter.findViewById(R.id.key)).setText(letters[position]);
		}
		else
		{
			Log.d("Letter","constructor2");
			gridLetter = (RelativeLayout) (convertView == null
	                ? letterInf.inflate(R.layout.letter_adapter, parent, false)
	                : convertView);
			((Button)gridLetter.findViewById(R.id.key)).setText(lettersBottom[position]);
		}
		
		return gridLetter;
	}

}
