package info.androidhive.slidingmenu;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PagesFragment extends Fragment {
	
	public PagesFragment(){}
	
	 private static final int PROGRESS = 0x1;

     private ProgressBar mProgress;
     private int mProgressPrimaryStatus = 0;
     private int mProgressSecondaryStatus = 0;

     private Handler mHandler = new Handler();
     TextView cContent;


	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_pages, container, false);
         
        return rootView;
    }
	
	 public void onActivityCreated(Bundle savedInstanceState) {
         // TODO Auto-generated method stub
         super.onActivityCreated(savedInstanceState);

         cContent = (TextView) getActivity().findViewById(R.id.txtLabel);


         mProgress = (ProgressBar) getActivity().findViewById(R.id.energyBar);

         mProgress.setProgress(mProgressPrimaryStatus);
         // Start lengthy operation in a background thread
         /*new Thread(new Runnable() {
             public void run() {
                 while (mProgressStatus < 100) {

                     // Update the progress bar
                     mHandler.post(new Runnable() {
                         public void run() {
                             mProgress.setProgress(mProgressStatus);
                         }
                     });
                     mProgressStatus = 50;
                     
                 }
             }
         }).start();*/
         
      
        	    View.OnClickListener handler = new View.OnClickListener(){
        	        public void onClick(View v) {
        	            //we will use switch statement and just
        	            //get thebutton's id to make things easier
        	            switch (v.getId()) {
        	 
        	                case R.id.btnYes: //toast will be shown
        	                    cContent.setText("Yes");
        	                    mProgressPrimaryStatus += 5;
        	                    mProgressSecondaryStatus += 5;
        	                    mProgress.setProgress(mProgressPrimaryStatus);
        	                    mProgress.setSecondaryProgress(mProgressSecondaryStatus);
        	                    break;
        	                case R.id.btnNo: //program will end
        	                	cContent.setText("No");
        	                	mProgressSecondaryStatus += 5;
        	                	mProgress.setSecondaryProgress(mProgressSecondaryStatus);
        	                    break;
        	            }
        	        }
        	    };
        	         
        	    //we will set the listeners
        	    getActivity().findViewById(R.id.btnYes).setOnClickListener(handler);
        	    getActivity().findViewById(R.id.btnNo).setOnClickListener(handler);
        	         
        	


     }    
}
