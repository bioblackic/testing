package info.androidhive.slidingmenu;

import java.util.ArrayList;
import java.util.Random;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FindPeopleFragment extends Fragment {
	
	public FindPeopleFragment(){}
	
	//the words
		private ArrayList<String> words = new ArrayList<String>();;
		//random for word selection
		private Random rand;
		//store the current word
		private String currWord;
		//the layout holding the answer
		private LinearLayout wordLayout;
		//text views for each letter in the answer
		private TextView[] charViews;
		//letter button grid
		private GridView letters;
		private GridView lettersBottom;
		//letter button adapter
		private LetterAdapter ltrAdapt;
		private LetterAdapter ltrAdapt2;
		//total hangmans
		private int numParts=6;
		//current part
		private int currPart;
		//num chars in word
		private int numChars;
		//num correct so far
		private int numCorr;
		//help
		private AlertDialog helpAlert;
		
		private TextView text;
	
	
	ImageView hangman1;
	TextView english;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_find_people, container, false);
         
        return rootView;
    }
	
	public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        init();
      //initialize random
      		rand = new Random();
      		//initialize word
      		currWord="";
      		
      	//get letter button grid
    		letters = (GridView)getActivity().findViewById(R.id.letters);
    		lettersBottom = (GridView)getActivity().findViewById(R.id.lettersBottom); 
    		playGame();
    }

	private void init() {
		// TODO Auto-generated method stub
		hangman1 = (ImageView) getActivity().findViewById(R.id.hangman);
		english = (TextView) getActivity().findViewById(R.id.english);

		//get answer area
				wordLayout = (LinearLayout)getActivity().findViewById(R.id.word);
		
		words.add("play");
		words.add("haha");
		words.add("apple");
		words.add("singing");
	}

	private void playGame(){

		//choose a word
		String newWord = words.get(rand.nextInt(words.size()));
		//make sure not same word as last time
		while(newWord.equals(currWord)) newWord = words.get(rand.nextInt(words.size()));
		//update current word
		currWord = newWord;
		
		//set the english word
		english.setText(currWord);

		//create new array for character text views
		charViews = new TextView[currWord.length()];

		//remove any existing letters
		wordLayout.removeAllViews();

		//loop through characters
		for(int c=0; c<currWord.length(); c++){
			charViews[c] = new TextView(getActivity());
			//set the current letter
			charViews[c].setText(""+currWord.charAt(c));
			//set layout
			charViews[c].setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 
					LayoutParams.WRAP_CONTENT));
			charViews[c].setGravity(Gravity.CENTER);
			charViews[c].setTextColor(Color.TRANSPARENT);
			charViews[c].setBackgroundResource(R.drawable.letter_bg);
			//add to display
			wordLayout.addView(charViews[c]);
			
		}

		//reset adapter
		ltrAdapt=new LetterAdapter(getActivity(),true);
		letters.setAdapter(ltrAdapt);
		ltrAdapt2=new LetterAdapter(getActivity(),false);
		lettersBottom.setAdapter(ltrAdapt2);

		//start part at zero
		currPart=0;
		//set word length and correct choices
		numChars=currWord.length();
		numCorr=0;

		
	}
	/*
	//letter pressed method
			public void letterPressed(View view){
				//find out which letter was pressed
				String ltr=((TextView)view).getText().toString();
				char letterChar = ltr.charAt(0);
				//disable view
				view.setEnabled(false);
				view.setBackgroundResource(R.drawable.letter_down);
				//check if correct
				boolean correct=false;
				for(int k=0; k<currWord.length(); k++){ 
					if(currWord.charAt(k)==letterChar){
						correct=true;
						numCorr++;
						charViews[k].setTextColor(Color.BLACK);
					}
				}
				//check in case won
				if(correct){
					if(numCorr==numChars){
						//disable all buttons
						disableBtns();
						//let user know they have won, ask if they want to play again
						
					}
				}
				//check if user still has guesses
				else if(currPart<numParts){
					//show next part
					switch(currPart){
					case 0:
					hangman1.setImageResource(R.drawable.hangman2);
					break;
					case 1:
					hangman1.setImageResource(R.drawable.hangman3);	
					break;
					case 2:
					hangman1.setImageResource(R.drawable.hangman4);
					break;
					case 3:
					hangman1.setImageResource(R.drawable.hangman5);
					break;
					case 4:
					hangman1.setImageResource(R.drawable.hangman6);
					break;
					case 5:
					hangman1.setImageResource(R.drawable.hangman7);
					break;
					default:
						
					}
					currPart++;
				}
				else{
					//user has lost
					disableBtns();
					
				}
			}
			*/

		//disable letter buttons
		public void disableBtns(){	
			int numLetters = letters.getChildCount();
			for(int l=0; l<numLetters; l++){
				letters.getChildAt(l).setEnabled(false);
			}
		}
}
